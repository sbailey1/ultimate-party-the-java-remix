<img src="https://media.giphy.com/avatars/gitlab/HyKFKml3EsoS.gif" alt="tanuki" width="300"/>

# 📝 Agenda: 

<details>
<summary>

## 🟠 Taking Out the Risk in Risky Business

</summary>

### ✅ Reduce Security Risks Quickly ✅ 

</details>

<details>
<summary>

## 🟣 Go Ultimate! From Security Scanners to Pipelines

</summary>

### ✅ Simplify Operations ✅ 

</details>
<details>
<summary>

## 🟠  Mastering the Merge Request

</summary>

### ✅ Build Collaborative Teams ✅ 

</details>

<details>
<summary>

## 🟣 Accelerating DevOps Success with DORA4

</summary>

### ✅ Achieve Visibility and DevOps Goals ✅ 

</details>
<details>
<summary>

## 🟠 Grab That Easy Button! with Templates

</summary>

## ✅ Increase Productivity ✅ 

</details>
<BR>

# GitLab DevOps Stages

 1. MANAGE 🗝️
 2. SECURE 🔒
 3. PLAN 📝
 4. RELEASE 🏃
 5. PACKAGE 🎁
 6. DEVOPS 🏎️
 7. CREATE 🎨
 8. VERIFY ✅
 9. CONFIGURE ⚙️
 10. PROTECT 🛡️


 ## Mermaid Example 🧜

 [![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcbiAgICBBW0NocmlzdG1hc10gLS0-fEdldCBtb25leXwgQihHbyBzaG9wcGluZylcbiAgICBCIC0tPiBDe1dJU0hMSVNUfVxuICAgIEMgLS0-fEFMRVh8IERbTGFwdG9wXVxuICAgIEMgLS0-fEFOTkF8IEVbaVBob25lXVxuICAgIEMgLS0-fEFSSUVMTEF8IEZbZmE6ZmEtY2FyIENhcl0iLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbiAgICBBW0NocmlzdG1hc10gLS0-fEdldCBtb25leXwgQihHbyBzaG9wcGluZylcbiAgICBCIC0tPiBDe1dJU0hMSVNUfVxuICAgIEMgLS0-fEFMRVh8IERbTGFwdG9wXVxuICAgIEMgLS0-fEFOTkF8IEVbaVBob25lXVxuICAgIEMgLS0-fEFSSUVMTEF8IEZbZmE6ZmEtY2FyIENhcl0iLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)

## SmartyPants

SmartyPants converts ASCII punctuation characters into "smart" typographic punctuation HTML entities. For example:

|                |ASCII                          |HTML                         |
|----------------|-------------------------------|-----------------------------|
|Single backticks|`'Isn't this fun?'`            |'Isn't this fun?'            |
|Quotes          |`"Isn't this fun?"`            |"Isn't this fun?"            |
|Dashes          |`-- is en-dash, --- is em-dash`|-- is en-dash, --- is em-dash|


## KaTeX

You can render LaTeX mathematical expressions using [KaTeX](https://khan.github.io/KaTeX/):

The *Gamma function* satisfying $\Gamma(n) = (n-1)!\quad\forall n\in\mathbb N$ is via the Euler integral

$$
\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.
$$

> You can find more information about **LaTeX** mathematical expressions [here](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference).


## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/). For example, this will produce a sequence diagram:

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

And this will produce a flow chart:

```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```

{"mode":"full","isActive":false}


### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
